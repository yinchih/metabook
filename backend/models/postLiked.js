const mongoose = require('mongoose');

const postLikedSchema = mongoose.Schema({
   post: { type: mongoose.Schema.Types.ObjectId, ref: "Post", required: true },
   dateCreate: { type: Date, default: Date.now },
   liker: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true }
});

module.exports = mongoose.model('PostLiked', postLikedSchema);
