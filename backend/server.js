// The only nodeJS start point placed in Angular app root directory
// The backend folder is the exact nodeJS root directory
// All npm installed package will be write in the angular's root folder package.json & stored the nodeJS needed package in the root node_modules folder

const http = require('http');
const app = require('./app');  // import the app.js where we include the express framework

const port = process.env.PORT || 3000;

app.set('port', port);

// http package is default package come with nodeJs.
// So http package doesn't included in the package.json & we don't need to manually 'npm install http' to make it work
const server = http.createServer(app);

// start the server
server.listen(port);
