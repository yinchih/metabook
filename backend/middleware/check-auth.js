const jwt = require('jsonwebtoken');

// directly export a function that verify the jwt token only
module.exports = (req, res, next) => {
  try {

    // split with space because we want to skip the prefix 'bearer' and get the exact token from header.authorization
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.verify(token, process.env.JWT_KEY);

    // remember we save the email & userId in payload when we create a token in Sign-in service.
    // we will get back the userId & store it in request & this middleware will call next() and pass the value to next middleware
    // the purpose of passing this userId around is to store it in the post db table to indicate who is the creator
    req.userData = { email: decodedToken.email, userId: decodedToken.userId }

    next();

  } catch (error) {
    res.status(401).json({
      message: "You're not authenticated!"
    });
  }
}
