const express = require('express');

const PostController = require("../controllers/posts");

const checkAuth = require('../middleware/check-auth');

const extractFile = require('../middleware/file');

const router = express.Router();

// insert post to mongodb, storage will be saved first before insert data into DB
router.post("", checkAuth, extractFile, PostController.createPost);

// update post
router.put("/:id", checkAuth, extractFile, PostController.updatePost);

// fetch all posts from mongodb without checking the token validation, pagination is applied
router.get("", PostController.getPosts);

// fetch single post by id without check the token validation
router.get('/:id', PostController.getPost);

// delete post from mongodb
router.delete('/:id', checkAuth, PostController.deletePost);

// insert like post
router.post("/like", checkAuth, PostController.likePost);

module.exports = router;
