const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const postsRoutes = require('./routes/posts');
const userRoutes = require('./routes/user');

// app is a big chain of middlewares we apply for the incoming requests
const app = express();

// mongoose connection return a Promise then() success method and catch() error method
const DB_CONNECTION_STRING_LIVE = "mongodb+srv://ian:" + process.env.MONGO_ATLAS_PW + "@meanbook-live.qhk39.mongodb.net/myFirstDatabase";
//const DB_CONNECTION_STRING_DEV = "mongodb+srv://ian:" + process.env.MONGO_ATLAS_PW + "@cluster0.trrih.mongodb.net/myFirstDatabase";
const DB_CONNECTION_STRING_DEV = "mongodb://localhost:27017/metabook?readPreference=primary&directConnection=true&ssl=false";

mongoose.connect(DB_CONNECTION_STRING_DEV)
  .then(() => {
    console.log('Connected to database!');
  }).catch(() => {
    console.log('Connection failed!');
  });

// define bodyParser in express framework that parse incoming request bodies in a middleware before your handlers
// data in request body will be easily extract & then conveniently access the data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// request going to images are actually forwarded to backend/images
app.use('/images', express.static(path.join('backend/images')));

// app.use() used to attach any type of middleware. Any request will go thru the middleware & do something or response to the request
// if a function is attached in app.use(), we need to have next() so will continue run on the next attached app.use(function)
app.use((req, res, next) => {

  // in response header set the CORS allow for cross domain service call
  res.setHeader("Access-Control-Allow-Origin", "*");

  // set to allow request header with content type & etc
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE, OPTIONS"
  );
  next();
});

// only URL start with /api/posts will be able to go thru the routes
app.use("/api/posts", postsRoutes);
app.use("/api/user", userRoutes);

// keep below 2 function if angular & nodejs deployment is on same server, delete it if it's on 2 diff server.
// this method used to forward all Angular static url request to Angular folder index.html. Not targeting any of the API route
// app.use("/", express.static(path.join(__dirname, 'AngularApp')));
// app.use((req, res, next) => {
//   res.sendFile(path.join(__dirname, "AngularApp", "index.html"));
// });

// export this configured express middleware as 'app'
module.exports = app;
