const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

exports.createUser = (req, res, next) => {

  bcrypt.hash(req.body.password, 10)
    .then( hash => {
      const user = new User({
        email: req.body.email,
        password: hash,
        fullname: req.body.fullname
      });
      user.save()
        .then(result => {
          res.status(201).json({
            message: "User created",
            result: result
          });
        })
        .catch(err => {
          res.status(500).json({
             message: "Invalid authentication credentials!"
          });
        });
    });
};

exports.userLogin = (req, res, next) => {

  let fetchedUser;

  User.findOne({ email: req.body.email })
    .then(user => {
      if (!user) {
        return res.status(401).json({
          message: "User not found!"
        });

        // todo: eventhough failed in email verification but will still procced to next then() promises
      } else {

        fetchedUser = user;

        // return boolean to next promise (next then)
        return bcrypt.compare(req.body.password, user.password);
      }
    })
    .then(result => {

      if (!result) {
        return res.status(401).json({
          message: "Password is wrong!"
        });
      }

      // sign method will create a token based on some input of your choice & secret key of your choice too
      const token = jwt.sign(
        { email: fetchedUser.email, userId: fetchedUser._id },
        process.env.JWT_KEY,
        { expiresIn: '12h' }
      );

      // 12 hours equals to 43200 seconds
      res.status(200).json({
        message: fetchedUser.email + " has been successfully signed in",
        token: token,
        expiresIn: 43200,
        userId: fetchedUser._id,
        userName: fetchedUser.fullname
      });
    })
    .catch(err => {
      return res.status(401).json({
        message: "Invalid authentication credentials!"
      });
    });
};
