const Post = require('../models/post');
const PostLiked = require('../models/postLiked');

exports.createPost = (req, res, next) => {

  const url = req.protocol + '://' + req.get('host');

  const post = new Post({
    title: req.body.title,
    content: req.body.content,
    imagePath: url + '/images/' + req.file.filename,
    creator: req.userData.userId
  });

  // after save will return post object
  // angular feature '...createdPost' will copy all same entity name & last we overwrite the _id from mongodb as id
  post.save()
    .then(createdPost => {
      res.status(201).json({
        message: 'Post added successfully',
        post: {
          ...createdPost,
          id: createdPost._id
        }
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Created a post failed!"
      });
    });
};

exports.updatePost = (req, res, next) => {

  // initialize string imagePath first which getting from Post model
  let imagePath = req.body.imagePath;

  // imagePath will changed if FormData image file object if found
  if (req.file) {
    const url = req.protocol + '://' + req.get('host');
    imagePath = url + '/images/' + req.file.filename;
  }

  const post = new Post({
    _id: req.body.id,
    title: req.body.title,
    content: req.body.content,
    imagePath: imagePath,
    creator: req.userData.userId
  });

  // update based on post id & creator
  Post.updateOne({ _id: req.params.id, creator: req.userData.userId }, post)
    .then(result => {
      if (result.n > 0) {
        res.status(200).json({ message: "Updated successfully" });
      } else {
        res.status(401).json({ message: "Not authorized!" });
      }
    }).catch(error => {
      res.status(500).json({
        message: "Couldn't update post!"
      });
    });
};

exports.getPosts = (req, res, next) => {

  // req.query always return string, we put a + mark to convert it to numeric
  const pageSize = +req.query.pagesize;
  const currentPage = +req.query.page;
  const profileId = req.query.userid;

  let postQuery;

  if (req.query.userid == '') {
    // default query just get all posts
    postQuery = Post.find().sort({dateCreate: 'desc'});
  } else {
    // query profileId post only
    postQuery = Post.find({creator: profileId}).sort({dateCreate: 'desc'});
  }

  let fecthedPosts;

  // define query according to page size & current page
  if (pageSize && currentPage) {
    postQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
  }

  // postQuery.find().populate('creator')
  // .exec(function (err, psts) {
  //   if (err) return handleError(err);
  //   console.log(psts);
  //   psts.forEach((pst, i) => {
  //     console.log('%d The creator is %s', i, pst.creator.fullname);
  //   });
  // });

  postQuery.populate('creator')
  .then(documents => {
    //console.log(documents);
    fecthedPosts = documents;
    return Post.count();
  })
  .then(count => {
    //console.log('total posts count: ' + count);

    res.status(200).json({
      message: 'Posts fetch successfully',
      posts: fecthedPosts,
      maxPosts: count
    });
  })
  .catch(error => {
    res.status(500).json({
      message: "Fetching post failed!"
    });
  });
};

exports.getPost = (req, res, next) => {

  Post.findById(req.params.id).then(post => {
    if (post) {
      res.status(200).json(post);
    } else {
      res.status(404).json({
        message: 'Post not found!'
      });
    }
  }).catch(error => {
    res.status(500).json({
      message: "Fetching post failed!"
    });
  });
};

exports.deletePost = (req, res, next) => {

  Post.deleteOne({ _id: req.params.id, creator: req.userData.userId })
    .then(result => {
      console.log(result);
      if (result.n > 0) {
        res.status(200).json({ message: "Post deleted successfully!" });
      } else {
        res.status(401).json({ message: "Not authorized!" });
      }
    }).catch(error => {
      res.status(500).json({
        message: "Fetching post failed!"
      });
    });
};

exports.likePost = (req, res, next) => {

  const postLiked = new PostLiked({
    post: req.body.postId,
    liker: req.body.userId
  });

  postLiked.save()
  .then(postLikedResult => {
    res.status(201).json({
      message: 'Post liked successfully',
      postLiked: {
        ...postLikedResult,
        id: postLikedResult._id
      }
    });
  })
  .catch(error => {
    res.status(500).json({
      message: "Post liked failed!"
    });
  });
};
