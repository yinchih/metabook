var httpStatus = {};

httpStatus.SUCCESS = 200;
httpStatus.NON_AUTHORITATIVE_INFORMATION = 203;
httpStatus.BAD_REQUEST = 400;
httpStatus.UNAUTHORIZED = 401;
httpStatus.FORBIDDEN = 403;
httpStatus.RECORD_NOT_FOUND = 404 ;
httpStatus.CONFLICT=409;
httpStatus.UNPROCESSABLE_ENTITY=422
httpStatus.INTERNAL_SERVER_ERROR = 500;

module.exports = httpStatus;
