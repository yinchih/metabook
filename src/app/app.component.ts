import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy{

  isExpanded = false;
  isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

  userIsAuthenticated = false;
  userName: string;
  userId: string;

  private authListenerSubs: Subscription;
  private loggedInUserNameListenerSub: Subscription;
  private loggedInUserIdListenerSub: Subscription;

  constructor(private authService: AuthService) {}

  ngOnInit() {

    this.authService.autoAuthUser();

    this.userIsAuthenticated = this.authService.getIsAuth();
    this.userName = this.authService.getUserName();
    this.userId = this.authService.getUserId();

    this.authListenerSubs = this.authService
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });

    this.loggedInUserNameListenerSub = this.authService
      .getLoggedInUserNameListener()
      .subscribe(userName => {
        this.userName = userName;
      });

      this.loggedInUserIdListenerSub = this.authService
      .getLoggedInUserIdListener()
      .subscribe(userId => {
        this.userId = userId;
      });
  }

  onLogout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
    this.loggedInUserNameListenerSub.unsubscribe();
    this.loggedInUserIdListenerSub.unsubscribe();
  }
}
