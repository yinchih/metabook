export interface AuthData {
  email: string;
  password: string;
  fullname: string;
}
