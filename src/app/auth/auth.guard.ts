import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";

// Angular add some interfaces your classes can implment which force the the classes to add certain method
// which the Angular/router can execute before it loads a route to check whether it should proceed or do somthing else.
// That interface which help us to protecting our routes is CanActive interface
@Injectable()
export class AuthGuard implements CanActivate {

constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {

    const isAuth = this.authService.getIsAuth();

    if (!isAuth) {
      this.router.navigate(['/auth/login']);
    }

    return isAuth;
  }

}
