import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

import { AuthData } from './auth-data.model';
import { environment } from 'src/environments/environment';

const BACKEND_URL = environment.apiUrl + "/user";

@Injectable({ providedIn: "root" })
export class AuthService {

  private isAuthenticated = false;

  private token: string;
  private tokenTimer: any;
  private userId: string;
  private userName: string;

  // use boolean type to indicate the authStatus is still valid or not
  private authStatusListener = new Subject<boolean>();
  private loggedInUserNameListener = new Subject<string>();
  private loggedInUserIdListener = new Subject<string>();

  constructor(private http: HttpClient, private router: Router) {}

  getToken() {
    return this.token;
  }

  getIsAuth() {
    return this.isAuthenticated;
  }

  getUserId() {
    return this.userId;
  }

  getUserName() {
    return this.userName;
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  getLoggedInUserNameListener() {
    return this.loggedInUserNameListener.asObservable();
  }

  getLoggedInUserIdListener() {
    return this.loggedInUserIdListener.asObservable();
  }

  createUser(email: string, password: string, fullname: string) {
    const authData: AuthData = { email: email, password: password, fullname: fullname };
    this.http
      .post(BACKEND_URL + "/signup", authData)
      .subscribe(() => {
        this.router.navigate(["/"]);
      }, error => {
        this.authStatusListener.next(false);
      });
  }

  login(email: string, password: string) {
    const authData: AuthData = { email: email, password: password, fullname: null };
    this.http
      .post<{ message: string, token: string, expiresIn: number, userId: string, userName: string }>(
        BACKEND_URL + "/login",
        authData
      )
      .subscribe( response => {

        const token = response.token;
        this.token = token;

        if (token) {
          const expiresInDuration = response.expiresIn;
          this.setAuthTimer(expiresInDuration);

          this.userId = response.userId;
          this.loggedInUserIdListener.next(response.userId);

          this.isAuthenticated = true;
          this.authStatusListener.next(true);

          this.userName = response.userName;
          this.loggedInUserNameListener.next(response.userName);

          // set token expiration to local storage
          const now = new Date();
          const expirationDate = new Date (now.getTime() + expiresInDuration * 1000);
          this.saveAuthDataInLocalStorage(token, expirationDate, this.userId, this.userName);

          console.log("expirationDate : " + expirationDate);
          this.router.navigate(['/']);
        }
      }, error => {
        this.authStatusListener.next(false);
      });
  }

  // this check user token method will be call when startup the application which is in app.component.ts
  // user will be auto logged-in if you re-open the application. token & data will be fetched from localstorage
  autoAuthUser() {
    const authInformation = this.getAuthDataFromLocalStorage();

    //console.log(authInformation);

    if (!authInformation) {
      return;
    }

    const now = new Date();
    const expiresIn = authInformation.expirationDate.getTime() - now.getTime();

    // token is valid if the the token expirationDate retrieved from browser local storage is in future time
    if (expiresIn > 0)
      this.token = authInformation.token;
      this.isAuthenticated = true;
      this.userId = authInformation.userId;
      this.userName = authInformation.userName;

      // set back the left over time
      this.setAuthTimer(expiresIn / 1000);

      // update observable auth status
      this.authStatusListener.next(true);
      this.loggedInUserNameListener.next(this.userName);
      this.loggedInUserIdListener.next(this.userId);
  }

  logout() {
    this.token = null;
    this.isAuthenticated = false;

    //push false auth status to all subscription
    this.authStatusListener.next(false);
    this.loggedInUserNameListener.next("");
    this.loggedInUserIdListener.next("");

    this.userId = null;
    this.userName = null;

    clearTimeout(this.tokenTimer);
    this.clearAuthDataFromLocalStorage();

    this.router.navigate(['/']);
  }

  private setAuthTimer(duration: number) {
    //console.log("Setting timer: " + duration);
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

  // getting token & expirationDate from brower local storage
  private getAuthDataFromLocalStorage() {
    const token = localStorage.getItem("token");
    const expirationDate = localStorage.getItem("expiration");
    const userId = localStorage.getItem("userId");
    const userName = localStorage.getItem("userName");

    if (!token || !expirationDate) {
      return;
    }

    return {
      token: token,
      expirationDate: new Date(expirationDate),
      userId: userId,
      userName: userName
    };
  }

  // just saving token & expirationDate in browser local storage. Might check in browser > Inspect > application > local storage > token
  private saveAuthDataInLocalStorage(token: string, expirationDate: Date, userId: string, userName: string) {
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expirationDate.toISOString());
    localStorage.setItem('userId', userId);
    localStorage.setItem('userName', userName);
  }

  private clearAuthDataFromLocalStorage() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
    localStorage.removeItem('userId');
    localStorage.removeItem('userName');
  }

}
