import { HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";

// Interceptor will injecting the token to http header. So during api call, we don't need to set Authorization header anymore
// we need empty injectable here so we can inject services into this service, it's a requirement of Angular
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  // this Interceptor class work like a middlewareclass,
  // just run for outgoing request instead of incoming request
  intercept(req: HttpRequest<any>, next: HttpHandler) {

    const authToken = this.authService.getToken();

    // better to clone the request to avoid unwanted bad result
    const authRequest = req.clone({
      // while cloning we also set with overwrite the Authorization header with token value
      headers: req.headers.set('Authorization', 'Bearer ' + authToken)
    });

    return next.handle(authRequest);
  }
}
