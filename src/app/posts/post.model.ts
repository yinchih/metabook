export interface Post {
  id: string;
  title: string;
  content: string;
  imagePath: string;
  dateCreate: Date;
  creator: string;
  creatorName: String;
}
