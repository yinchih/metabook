import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';
import { Post } from './post.model';
import { PostLiked } from './postLiked.model';

const BACKEND_URL = environment.apiUrl + "/posts/";

// Instead of include this in the app.module under Providers to let angular scan & find this service,
// we just need to use @Injectable will do the same purposes. With providedIn root will create a singleton instance only.
@Injectable({providedIn: 'root'})
export class PostsService {

  private posts: Post[] = [];
  private postsUpdated = new Subject<{posts: Post[], postCount: number}>();

  constructor(private http: HttpClient, private router: Router) {}

  getPosts(postsPerPage: number, currentPage: number, userId: string) {

    const queryParams = `?pagesize=${postsPerPage}&page=${currentPage}&userid=${userId}`;

    //console.log('postsPerPage : ' + postsPerPage + " / currentpage : " + currentPage);

    //return posts array in secure way, [...] syntax means references only with address pointing to the private post array
    //return [...this.posts] will send a copy of the post;
    this.http
      .get<{message: String, posts: any, maxPosts: number }>(
        BACKEND_URL + queryParams
      )
      .pipe(map((postData) => {
        // pipe mapping mainly to map all Posts mongodb's _id to local id attribute
        return { posts: postData.posts.map(post => {

          return {
            id: post._id,
            title: post.title,
            content: post.content,
            imagePath: post.imagePath,
            dateCreate: post.dateCreate,
            creator: post.creator._id,
            creatorName: post.creator.fullname
          };
        }), maxPosts: postData.maxPosts};
      }))
      .subscribe((transformedPostData) => {
        this.posts = transformedPostData.posts;
        this.postsUpdated.next({
          posts: [...this.posts],
          postCount: transformedPostData.maxPosts
        });
      });
  }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }

  getPost(id: string) {
    return this.http.get<{
      _id: string,
      title: string,
      content: string,
      imagePath: string,
      dateCreate: Date,
      creator: string
    }>(BACKEND_URL + id);
  }

  likePost(userId: string, postId: string) {

    const body = {userId: userId, postId: postId};
    return this.http.post<{message: string, postLiked: PostLiked}>(BACKEND_URL + "like", body)
    .subscribe((responseData) => {
      console.log(responseData);
    });
  }

  addPost(title: string, content: string, image: File) {
    // Discard this json pattern because we need to attached image now
    // const post: Post = { id: null, title: title, content: content };
    // this.http.post<{message: string, postId: string}>('http://localhost:3000/api/posts', post)

    // We use FormData instead of post model when image need to be attached.
    // FormData is a js default form object that accept text data & blob
    const postData = new FormData();
    postData.append("title", title);
    postData.append("content", content);
    postData.append("image", image, title);

    // http post postData to nodeJS server & save there
    // angular will auto detect & bind the postData right header. E.g. title & content
    this.http.post<{message: string, post: Post}>(BACKEND_URL, postData)
      .subscribe((responseData) => {
        // redirect back to post listing page
        this.router.navigate(['/']);
    });
  }

  updatePost(id: string, title: string, content: string, image: File | string) {

    //const post: Post = { id: id, title: title, content: content, imagePath: null };

    // postData argument can be Post model or FormData
    let postData: Post | FormData;

    // if image is File object will send FormData else will send Post model
    if (typeof(image) === 'object') {
      postData = new FormData();
      postData.append("id", id);
      postData.append("title", title);
      postData.append("content", content);
      postData.append("image", image, title);
    } else {
      // we put creator as null in client side to avoid manipulation.
      // creator will be assigned in server side later, dateCreate null value will be updated as new Date in server side
      postData = {
        id: id,
        title: title,
        content: content,
        imagePath: image,
        dateCreate: null,
        creator: null,
        creatorName: null,
      };
    }

    this.http.put(BACKEND_URL + id, postData)
      .subscribe(response => {

        // update the subsribed posts in memory with the updated post
        // const updatedPosts = [...this.posts];
        // const oldPostIndex = updatedPosts.findIndex(p => p.id = id);
        // const post: Post = {
        //   id: id,
        //   title: id,
        //   content: content,
        //   imagePath: ""
        // };
        // updatedPosts[oldPostIndex] = post;
        // this.posts = updatedPosts;
        // this.postsUpdated.next([...this.posts]);

        this.router.navigate(['/']);
      });
  }

  deletePost(postId: string) {
    return this.http.delete(BACKEND_URL + postId);
  }
}
