import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';

import { Post } from '../post.model';
import { PostsService } from '../posts.service';
import { AuthService } from 'src/app/auth/auth.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})

export class PostListComponent implements OnInit, OnDestroy {

  posts: Post[] = [];

  totalPost = 0;
  postsPerPage = 10;
  currentPage = 1;
  pageSizeOptions = [1, 2, 5, 10];

  isLoading = false;
  userIsAuthenticated = false;
  userId: string;

  paramUserId: string;
  pageHeaderTitle: string = "All Public Post";
  pageHeaderIconName: string = "dashboard";

  private postsSub: Subscription;
  private authStatusSub: Subscription;

  constructor(
    public postsService: PostsService,
    private authService: AuthService,
    public route: ActivatedRoute,
    private router: Router
    ) {}

  ngOnInit() {

    this.isLoading = true;

    this.route.paramMap.subscribe((paramMap: ParamMap) => {

      if (paramMap.has('userId')) {
        this.postsService.getPosts(this.postsPerPage, this.currentPage, paramMap.get('userId'));
        this.paramUserId = paramMap.get('userId');

        //console.log("--> AuthService userid: " + this.authService.getUserId());
        //console.log("--> paramMap userid: " + paramMap.get('userId'));
      } else {
        this.postsService.getPosts(this.postsPerPage, this.currentPage, '');
      }
    });

    this.userId = this.authService.getUserId();

    this.postsSub = this.postsService
      .getPostUpdateListener()
      .subscribe((postData: {posts: Post[], postCount: number}) => {

        this.isLoading = false;
        this.totalPost = postData.postCount;
        this.posts = postData.posts;

        // Update the page header username & icon after we got posts updated from listener
        if (this.paramUserId == this.userId) {
          this.pageHeaderTitle = "My Posts";
          this.pageHeaderIconName = "perm_contact_calendar";
        } else if (this.paramUserId && this.paramUserId != this.userId) {
          this.pageHeaderTitle = this.posts[0].creatorName + " Posts";
          this.pageHeaderIconName = "account_circle";
        }
      });

    // this indicator will be used to show EDIT & DELETE button after we login
    this.userIsAuthenticated = this.authService.getIsAuth();

    // this sub is for logout purposes
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
        this.userId = this.authService.getUserId();
      });
  }

  onChangedPage(pageData: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.postsPerPage = pageData.pageSize;
    this.postsService.getPosts(this.postsPerPage, this.currentPage, '');
  }

  onDelete(postId: string) {
    this.isLoading = true;
    this.postsService.deletePost(postId).subscribe(
      (result) => {
        this.testSubscribeOnSuccess(result);
        this.postsService.getPosts(this.postsPerPage, this.currentPage, '');
      },
      error => {
        this.isLoading = false;
        this.testSubscribeOnError(error);
      },
      () => this.testSubscribeOnAlwaysRun()

    );
  }

  onLikePost(postId: string) {
    if (this.userIsAuthenticated)
      this.postsService.likePost(this.userId, postId);

  }

  ngOnDestroy() {
    // must unsubscribe all the subscription when onDestroy to prevent memory leak
    this.postsSub.unsubscribe();
    this.authStatusSub.unsubscribe();
  }

  // Testing code
  testSubscribeOnSuccess(result: any) {
    console.log("=====> delete onSuccess with message: " + result.message + " <=====");
  }

  testSubscribeOnError(error: any) {
    console.log("=====> delete onError with message: " + error.message + " <=====");
  }

  testSubscribeOnAlwaysRun() {
    console.log("=====> delete always run in this block <=====");
  }
}
