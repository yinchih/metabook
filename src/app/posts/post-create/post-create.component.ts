import { Component, OnInit, EventEmitter, Output, OnDestroy } from "@angular/core";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from "@angular/router";
import { Subscription } from "rxjs";

import { Post } from "../post.model";
import { AuthService } from "src/app/auth/auth.service";
import { PostsService } from "../posts.service";
import { mimeType } from "./mime-type.validator";
import { fileSizeValidator } from './file-size.validator';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})

export class PostCreateComponent implements OnInit, OnDestroy {

  public post: Post;
  public isLoading = false;

  form: FormGroup;
  imagePreview: string;

  public mode = 'create';
  private postId: string;

  private authStatusSub: Subscription;

  constructor(
    public postsService: PostsService,
    public route: ActivatedRoute,
    private authService: AuthService
  ) { }

  ngOnInit() {

    this.authStatusSub = this.authService.getAuthStatusListener()
      .subscribe(authStatus => {
        this.isLoading = false;
      });

    this.form = new FormGroup({
        title: new FormControl(null, {validators: [Validators.required, Validators.minLength(3)]}),
        content: new FormControl(null, {validators: [Validators.required]}),
        image: new FormControl(null, {
          validators: [Validators.required],
          asyncValidators: [mimeType, fileSizeValidator]
        })
    });

    // subscribe to observe & listen to the changes in the route url (postId parameter)
    // built in subscription mean that we doesn't need to unsubcribe ourself. Angular will handle that automatically
    this.route.paramMap.subscribe((paramMap: ParamMap) => {

      if (paramMap.has('postId')) {
        this.mode = 'edit';
        this.postId = paramMap.get('postId');

        this.isLoading = true;

        // subscribe to update the post data on UI whenever you reload the browser page
        this.postsService.getPost(this.postId).subscribe(postData => {
          this.isLoading = false;
          this.post = {
            id: postData._id,
            title: postData.title,
            content: postData.content,
            imagePath: postData.imagePath,
            dateCreate: postData.dateCreate,
            creator: postData.creator,
            creatorName: null,
          };
          this.form.setValue({
            title: this.post.title,
            content: this.post.content,
            image: this.post.imagePath
          });
        });
      } else {
        this.mode = 'create';
        this.postId = null;
      }
    });
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];

    // set image with file object
    this.form.patchValue({image: file});

    // inform angular that image has changed & should re-evaluate that, validate the image & run the validator again
    this.form.get('image').updateValueAndValidity();

    // bind preview thumbsnail img to a reader & read the image file
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    }
    reader.readAsDataURL(file);
  }

  onSavePost() {

    if (this.form.invalid) {
      return;
    }

    this.isLoading = true;

    if (this.mode === 'create') {
      this.postsService.addPost(
        this.form.value.title,
        this.form.value.content,
        this.form.value.image
        );
    } else {
      this.postsService.updatePost(
        this.postId,
        this.form.value.title,
        this.form.value.content,
        this.form.value.image
        );
    }

    this.form.reset();
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }
}
