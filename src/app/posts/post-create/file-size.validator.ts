import { AbstractControl, FormControl } from "@angular/forms";
import { Observable, Observer, of } from "rxjs";

export const fileSizeValidator = (
  control: AbstractControl
  ): Promise<{ [key: string]: any }> | Observable<{ [key: string]: any }> => {

    // when updating post without upload a new image, we will detect whether the value is string.
    // If string will direct return null means it is valid
    if (typeof(control.value) === 'string') {
      return of(null);
    }

    const file = control.value;
    const fileReader = new FileReader();

    const frObs = Observable.create((observer: Observer<{ [key: string]: any }>) => {

      fileReader.addEventListener("loadend", () => {

        // validation start here
        if (file) {

          const fileSize = file.size;
          const fileSizeInKB = Math.round(fileSize / 1024);

          // invalid if greater than 6mb
          if (fileSizeInKB >= 6000) {
            console.log("===> invalid file size: " + fileSizeInKB + ". Max 6mb.");
            observer.next({ invalidFileSize: true });
          } else {
            console.log("===> valid file size: " + fileSizeInKB + ". Max 6mb.");
            observer.next(null);
          }
          observer.complete();
        }

      });

      fileReader.readAsArrayBuffer(file);
    });

    return frObs;
};
