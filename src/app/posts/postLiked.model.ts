export interface PostLiked {
  id: string;
  post: string;
  dateCreate: Date;
  liker: string;
}
