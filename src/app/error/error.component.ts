import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  templateUrl: './error.component.html'
})

export class ErrorComponent {

  // to pass in the error message from error-interceptor, we use Inject() decorator
  // specify dependency MAT_DIALOG_DATA
  constructor(@Inject(MAT_DIALOG_DATA) public data: {message: string}) {}
}
