import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit, OnDestroy{

  @Input('inputSideNav') inputSideNav: MatSidenav;

  userIsAuthenticated = false;
  userId: string;
  userName: string;

  isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

  private authListenerSubs: Subscription;
  private loggedInUserNameListenerSub: Subscription;
  private loggedInUserIdListenerSub: Subscription;

  constructor(private authService: AuthService) {}

  ngOnInit() {

    // When we close & re-open browser, header OnInit should get token validation earlier
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.userName = this.authService.getUserName();
    this.userId = this.authService.getUserId();

    this.authListenerSubs = this.authService
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });

    this.loggedInUserNameListenerSub = this.authService
      .getLoggedInUserNameListener()
      .subscribe(userName => {
        this.userName = userName;
      });

    this.loggedInUserIdListenerSub = this.authService
      .getLoggedInUserIdListener()
      .subscribe(userId => {
        this.userId = userId;
      });
  }

  onLogout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
    this.loggedInUserNameListenerSub.unsubscribe();
    this.loggedInUserIdListenerSub.unsubscribe();
  }

}
