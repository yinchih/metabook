import { HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { ErrorComponent } from "./error/error.component";

// a global error handling to catch HTTP request error for signup, login... etc
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private dialog: MatDialog) {}

  // every outgoing HTTP request will be watched by this middleware and
  // if we get back an error response, this interceptor should kick in
  intercept(req: HttpRequest<any>, next: HttpHandler) {

    // handle give us back the response observable stream
    // use pipe to add catch error operator
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {

      // do your error handling here
        let errorMessage = "An unknown error occurred!";
        if (error.error.message) {
          errorMessage = error.error.message;
        }

        // open dialog component
        this.dialog.open(ErrorComponent, {data: {message: errorMessage}});

        // need to return an observable
        return throwError(error);
      })
    );
  }
}
