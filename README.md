# README 

## About METABOOK and Features 
 
METABOOK application is a simple social media single page web application (SPA) written by AngularJS. 

This project was created using the MEAN stack (MongoDB, Express.js, AngularJS, and Node.js) javascript framework on year 2021. 

Metabook includes the following features: 

1. Responsive layout adjusted based on pc browser and mobile browser. 
2. User account creation. 
3. Login & logout the application. 
4. Upload a photo & write a description in the post. 
5. View your own posts, public posts or certain user's posts. 
6. Edit own post by re-upload the photo or delete the post. 
 
Following items are pending features/enhancements to be developed: 

1. Like & comments of post by user. 
2. Private or public post option. 
3. Allow user to change their login password. 
4. optimize upload file size. 
5. Enlarge the description font. 
6. Allow post that doesn't have image upload. 

[Metabook Demo Website](http://meanbook.s3-website.us-east-2.amazonaws.com/)

## METABOOK User Interface

### Desktop Browser
![Desktop Browser](https://bitbucket.org/yinchih/metabook/raw/f3f3024476ede37c498c144ba12750081d0e4d50/src/assets/images/metabook-desktop-ui.jpg)

### Mobile Browser
![Desktop Browser](https://bitbucket.org/yinchih/metabook/raw/f3f3024476ede37c498c144ba12750081d0e4d50/src/assets/images/metabook-mobile-ui.jpg)

## IDE & Necessary Tools for Development 
 
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.6. 

Suggested to open the source code by using Visual Studio Code since it's a powerful and light weight IDE. 
 
Required tools: 

1. [Visual Studio Code](https://code.visualstudio.com/) - Main IDE
2. [NodeJS](https://nodejs.org/en/) - Back end javascript framework 
3. [Angular CLI](https://angular.io/cli) - Front end CLI  
4. [MongoDB Database](https://www.mongodb.com/) - Local version or MongoDB Atlas Cloud   
5. [MongoDB Compass](https://www.mongodb.com/products/compass) or [Robo 3T](https://robomongo.org/) - MongoDB Database GUI 

## Setup and Deploy Metabook on Local PC 
 
1. Make sure all `NodeJS`, `NPM`, `Angular CLI`, Java JDK, & MongoDB are pre-installed in your local PC.
2. Run `npm i` to install all front-end angular necessary nodejs packages/modules in metabook root folder. Check the package.json to see the dependencies of all packages.
3. Run `npm i` to install all back-end express necessary nodejs packages/modules in metabook/backend folder. Check the package.json to see the dependencies of all packages.
4. Run `ng serve` in the VScode IDE terminal to start the Angular client front end service. Port 4201 will be used.

```python
C:\metabook> ng serve
Browser application bundle generation complete.

Initial Chunk Files            | Names         |      Size
vendor.js                      | vendor        |   4.63 MB
polyfills.js                   | polyfills     | 510.54 kB
styles.css, styles.js          | styles        | 460.52 kB
main.js                        | main          | 135.07 kB
runtime.js                     | runtime       |  12.54 kB

                               | Initial Total |   5.72 MB

Lazy Chunk Files               | Names         |      Size
src_app_auth_auth_module_ts.js | -             |  28.72 kB

Build at: 2021-11-17T03:22:59.477Z - Hash: deb21fdb11b4de76ec2a - Time: 26279ms

Angular Live Development Server is listening on localhost:4201, open your browser on http://localhost:4201/
```

5. Run `npm run start:server` in the VScode IDE terminal to start the NodeJS back end service. Port 3000 will be used.

```python
C:\metabook>npm run start:server

> metabook@0.0.0 start:server
> nodemon ./backend/server.js

[nodemon] 2.0.12
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node ./backend/server.js`
Connected to database!
```

6. Navigate to [http://localhost:4201/](http://localhost:4201/) to launch the METABOOK landing page.
7. While startup nodeJS, database named `metabook` will be automatically created in your local mongoDB.
8. Please take note that backend Node Server code has located inside the Angular Client folder. This is for easy maintaining & development purposes. Suggested to separate the server & client code in production environment.
 
## Main Node Modules to be Installed 
 
1. angular (form/material/cdk/interceptor...)
2. express (web app framework)
3. mongoose (mongo modeling tool)
4. body-parser (request middleware)
5. bycrypt (encryption)
6. jsonwebtoken (authentication token)
7. multer (image upload)
8. rxjs (Reactive library for call stack & observable)
9. typescript (a superset of js & primary language for angular app)
10. nodemon (development used for auto restart node app) 
 
## Project Structure 
 
1. Angular code - located in metabook root folder
2. NodeJS code - located in metabook/backend folder
3. Uploaded images - located in metabook/backend/images folder
4. NodeJS port - metabook/backend/server.js
5. Angular port - metabook/angular.json
6. Mongo DB connection string & DB name - metabook/backend/app.js
7. Environment variable (port, mongo & jwt_key)- metabook/nodemon.json
8. Angular client API URL - metabook/src/environments/environment.ts. Environment file will be replaced automatically with prod.ts while running `ng build` 
 
## Development server 
 
Run `ng serve` for a dev server. Navigate to [http://localhost:4201/](http://localhost:4201/). The app will automatically reload if you change any of the source files.
 
## Build Project 
 
To generate the static angular files for production deployment, run `ng build` to build the project. The build artifacts will be stored in the `metabook/backend/angular_dist/` directory. 
 
## Production Server 
 
Suggested to deploy the backend web server code in AWS Elastic BeanStalk and host the angular client static code in AWS S3 Bucket Cloud Storage. Both services provided by Amazon AWS are free of charge. 
 
## Further Help 
 
To get more help on running this metabook application, kindly contact Yin Chih at teeyinchih@gmail.com or whatsapp +6012-6278314 
